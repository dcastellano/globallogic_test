package com.test.gltest.presentation.adapter.presenter

import com.squareup.picasso.Picasso
import com.test.gltest.presentation.adapter.holder.LaptopView
import com.test.gltest.presentation.callback.LaptopItemCallback
import com.test.gltest.presentation.model.LaptopItem
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class LaptopHolderPresenterTest {

    @Mock
    lateinit var callback: LaptopItemCallback

    @Mock
    lateinit var picasso: Picasso

    @Mock
    lateinit var view: LaptopView

    private lateinit var presenter: LaptopHolderPresenter

    private fun <T> uninitialized(): T = null as T

    private fun <T> anyObj(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    private fun getItem(title: String = "", description: String = "", imageUrl: String = "") =
        LaptopItem(title, description, imageUrl)

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = LaptopHolderPresenter(callback, picasso)
    }

    @Test
    fun bind_titleIsEmpty_callHideTitle() {
        val item = getItem(description = "description", imageUrl = "https:....")

        presenter.bind(view, item)

        verify(view, never()).setTitle(anyString())
        verify(view).hideTitle()
    }

    @Test
    fun bind_titleIsNotEmpty_callSetTitle() {
        val item = getItem(title = "title", description = "description", imageUrl = "https:....")

        presenter.bind(view, item)

        verify(view).setTitle(anyString())
        verify(view, never()).hideTitle()
    }

    @Test
    fun bind_descriptionIsEmpty_callHideShortDescription() {
        val item = getItem(title = "title", imageUrl = "https:....")

        presenter.bind(view, item)

        verify(view, never()).setShortDescription(anyString())
        verify(view).hideShortDescription()
    }

    @Test
    fun bind_descriptionIsNotEmpty_callSetShortDescription() {
        val item = getItem(title = "title", description = "description", imageUrl = "https:....")

        presenter.bind(view, item)

        verify(view).setShortDescription(anyString())
        verify(view, never()).hideShortDescription()
    }

    @Test
    fun bind_imageUrlIsEmpty_callHideImage() {
        val item = getItem(title = "title", description = "description")

        presenter.bind(view, item)

        verify(view, never()).loadImage(anyObj(), anyString())
        verify(view).hideImage()
    }

    @Test
    fun bind_imageUrlIsNotEmpty_callLoadImage() {
        val item = getItem(title = "title", description = "description", imageUrl = "https:....")

        presenter.bind(view, item)

        verify(view).loadImage(anyObj(), anyString())
        verify(view, never()).hideImage()
    }

    @Test
    fun bind_setOnItemClickListener() {
        val item = getItem(title = "title", description = "description", imageUrl = "https:....")

        presenter.bind(view, item)

        verify(view).setOnItemClickListener(anyObj())
    }

    @Test
    fun areItemsTheSame_differentTitle_returnFalse() {
        val newItem = getItem(title = "title1", description = "description", imageUrl = "https:....")
        val oldItem = getItem(title = "title2", description = "description", imageUrl = "https:....")

        val result = presenter.areItemsTheSame(newItem, oldItem)

        assertEquals(result, false)
    }

    @Test
    fun areItemsTheSame_sameTitle__returnTrue() {
        val newItem = getItem(title = "title1", description = "description", imageUrl = "https:....")
        val oldItem = getItem(title = "title1", description = "description", imageUrl = "https:....")

        val result = presenter.areItemsTheSame(newItem, oldItem)

        assertEquals(result, true)
    }

    @Test
    fun areContentsTheSame_differentDescriptionAndSameImageUrl_returnFalse() {
        val newItem = getItem(title = "title1", description = "description1", imageUrl = "https:....")
        val oldItem = getItem(title = "title2", description = "description2", imageUrl = "https:....")

        val result = presenter.areContentsTheSame(newItem, oldItem)

        assertEquals(result, false)
    }

    @Test
    fun areContentsTheSame_differentImageUrlAndSameDescription_returnFalse() {
        val newItem = getItem(title = "title1", description = "description", imageUrl = "https:....1")
        val oldItem = getItem(title = "title2", description = "description", imageUrl = "https:....2")

        val result = presenter.areContentsTheSame(newItem, oldItem)

        assertEquals(result, false)
    }

    @Test
    fun areContentsTheSame_sameImageUrlAndSameDescription_returnTrue() {
        val newItem = getItem(title = "title1", description = "description", imageUrl = "https:....")
        val oldItem = getItem(title = "title1", description = "description", imageUrl = "https:....")

        val result = presenter.areContentsTheSame(newItem, oldItem)

        assertEquals(result, true)
    }
}