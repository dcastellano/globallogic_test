package com.test.gltest.extension

import android.view.View

/**
 * Set the view to VISIBLE state
 */
fun View.makeItVisible() {
    visibility = View.VISIBLE
}

/**
 * Set the view to INVISIBLE state
 * This view is invisible, but it still takes up space for layout purposes
 */
fun View.makeItInvisible() {
    visibility = View.INVISIBLE
}

/**
 * Set the view to GONE state
 * This view is invisible, and it doesn't take any space for layout purposes
 */
fun View.makeItGone() {
    visibility = View.GONE
}