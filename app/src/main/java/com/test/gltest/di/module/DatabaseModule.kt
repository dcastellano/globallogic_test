package com.test.gltest.di.module

import android.content.Context
import androidx.room.Room
import com.test.gltest.repository.local.LocalDataBase
import com.test.gltest.repository.local.dao.LaptopDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(context: Context): LocalDataBase =
        Room.databaseBuilder(
            context,
            LocalDataBase::class.java,
            "test_db"
        )
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideLaptopDao(db: LocalDataBase): LaptopDao =
        db.laptopDao
}