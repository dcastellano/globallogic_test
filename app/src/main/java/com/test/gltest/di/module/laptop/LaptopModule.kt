package com.test.gltest.di.module.laptop

import androidx.lifecycle.ViewModel
import com.test.gltest.di.module.ViewModelKey
import com.test.gltest.domain.interactor.LaptopDetailInteractor
import com.test.gltest.domain.interactor.LaptopDetailInteractorImpl
import com.test.gltest.domain.interactor.LaptopInteractor
import com.test.gltest.domain.interactor.LaptopInteractorImpl
import com.test.gltest.mapper.LaptopMapper
import com.test.gltest.mapper.LaptopMapperImpl
import com.test.gltest.presentation.viewmodel.LaptopDetailViewModel
import com.test.gltest.presentation.viewmodel.LaptopViewModel
import com.test.gltest.repository.local.dao.LaptopDao
import com.test.gltest.repository.manager.laptop.LaptopRepoHelper
import com.test.gltest.repository.manager.laptop.LaptopRepoManager
import com.test.gltest.repository.manager.laptop.LaptopRepoManagerImpl
import com.test.gltest.repository.service.RetrofitService
import com.test.gltest.util.Util
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class LaptopModule {

    @Provides
    @IntoMap
    @ViewModelKey(LaptopViewModel::class)
    fun provideLaptopViewModel(interactor: LaptopInteractor): ViewModel {
        return LaptopViewModel(interactor)
    }

    @Provides
    fun provideLaptopInteractor(
        repository: LaptopRepoManager,
        mapper: LaptopMapper
    ): LaptopInteractor {
        return LaptopInteractorImpl(repository, mapper)
    }

    @Provides
    @IntoMap
    @ViewModelKey(LaptopDetailViewModel::class)
    fun provideLaptopDetailViewModel(interactor: LaptopDetailInteractor): ViewModel {
        return LaptopDetailViewModel(interactor)
    }

    @Provides
    fun provideLaptopDetailInteractor(
        repository: LaptopRepoManager,
        mapper: LaptopMapper
    ): LaptopDetailInteractor {
        return LaptopDetailInteractorImpl(repository, mapper)
    }

    @Provides
    fun provideLaptopRepoHelper(
        service: RetrofitService,
        dao: LaptopDao,
        mapper: LaptopMapper
    ): LaptopRepoHelper {
        return LaptopRepoHelper(service, dao, mapper)
    }

    @Provides
    fun provideLaptopRepoManager(
        util: Util,
        helper: LaptopRepoHelper
    ): LaptopRepoManager {
        return LaptopRepoManagerImpl(util, helper)
    }

    @Provides
    fun provideLaptopMapper(): LaptopMapper {
        return LaptopMapperImpl()
    }
}