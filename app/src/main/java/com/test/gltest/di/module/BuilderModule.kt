package com.test.gltest.di.module

import com.test.gltest.presentation.activity.LaptopActivity
import com.test.gltest.presentation.activity.LaptopDetailActivity
import com.test.gltest.presentation.fragment.LaptopDetailFragment
import com.test.gltest.presentation.fragment.LaptopFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuilderModule {

    @ContributesAndroidInjector
    abstract fun bindLaptopActivity(): LaptopActivity

    @ContributesAndroidInjector
    abstract fun bindLaptopFragment(): LaptopFragment

    @ContributesAndroidInjector
    abstract fun bindLaptopDetailActivity(): LaptopDetailActivity

    @ContributesAndroidInjector
    abstract fun bindLaptopDetailFragment(): LaptopDetailFragment
}