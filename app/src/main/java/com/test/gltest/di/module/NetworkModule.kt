package com.test.gltest.di.module

import com.test.gltest.repository.service.RetrofitService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import javax.net.ssl.HostnameVerifier

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofitService(): RetrofitService {
        val timeout = 20L
        val client = OkHttpClient().newBuilder()
            .connectTimeout(timeout, TimeUnit.SECONDS)
            .readTimeout(timeout, TimeUnit.SECONDS)
            .writeTimeout(timeout, TimeUnit.SECONDS)
            .hostnameVerifier(HostnameVerifier { _, _ -> true })
            .build()

        return Retrofit.Builder().client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://private-f0eea-mobilegllatam.apiary-mock.com/").build()
            .create(RetrofitService::class.java)
    }
}