package com.test.gltest.di.component

import android.app.Application
import android.content.Context
import com.test.gltest.app.TestApplication
import com.test.gltest.di.module.*
import com.test.gltest.di.module.laptop.LaptopModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        BuilderModule::class,
        UtilModule::class,
        NetworkModule::class,
        DatabaseModule::class,
        ViewModelFactoryModule::class,
        LaptopModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }

    fun inject(testApplication: TestApplication)
}