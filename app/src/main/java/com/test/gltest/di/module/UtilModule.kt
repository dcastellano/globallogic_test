package com.test.gltest.di.module

import android.content.Context
import com.test.gltest.util.Util
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UtilModule {

    @Provides
    @Singleton
    fun provideUtil(context: Context): Util {
        return Util(context)
    }
}