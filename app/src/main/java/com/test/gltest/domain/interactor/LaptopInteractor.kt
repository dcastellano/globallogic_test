package com.test.gltest.domain.interactor

import com.test.gltest.presentation.model.LaptopItem

interface LaptopInteractor {
    val shortDescriptionMaxChar: Int

    @Throws(Exception::class)
    suspend fun getLaptopList(): List<LaptopItem>
}