package com.test.gltest.domain.interactor

import com.test.gltest.presentation.model.LaptopItem

interface LaptopDetailInteractor {
    @Throws(Exception::class)
    suspend fun getLaptop(title: String): LaptopItem
}