package com.test.gltest.domain.interactor

import com.test.gltest.mapper.LaptopMapper
import com.test.gltest.presentation.model.LaptopItem
import com.test.gltest.repository.manager.laptop.LaptopRepoManager
import javax.inject.Inject

class LaptopDetailInteractorImpl @Inject constructor(
    private val repository: LaptopRepoManager,
    private val mapper: LaptopMapper
) : LaptopDetailInteractor {

    override suspend fun getLaptop(title: String): LaptopItem {
        val result = repository.getLaptop(title)
        return result.entity?.let { item -> mapper.toPresentation(item) }
            ?: throw result.exception?.let { it } ?: Exception("Unknown error")
    }
}