package com.test.gltest.domain.interactor

import com.test.gltest.mapper.LaptopMapper
import com.test.gltest.presentation.model.LaptopItem
import com.test.gltest.repository.manager.laptop.LaptopRepoManager
import javax.inject.Inject

class LaptopInteractorImpl @Inject constructor(
    private val repository: LaptopRepoManager,
    private val mapper: LaptopMapper
) : LaptopInteractor {

    override val shortDescriptionMaxChar = 50

    override suspend fun getLaptopList(): List<LaptopItem> {
        val result = repository.getLaptopList()
        result.entity?.let { list ->
            if (list.isEmpty())
                throw Exception("Empty laptop list")
            return mapper.toPresentation(list, shortDescriptionMaxChar)
        }

        throw result.exception?.let { it } ?: Exception("Unknown error")
    }
}