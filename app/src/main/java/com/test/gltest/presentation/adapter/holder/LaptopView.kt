package com.test.gltest.presentation.adapter.holder

import com.squareup.picasso.Picasso

interface LaptopView {
    fun setTitle(text: String)
    fun setShortDescription(text: String)
    fun loadImage(picasso: Picasso, url: String)

    fun hideTitle()
    fun hideShortDescription()
    fun hideImage()

    fun setOnItemClickListener(onClickFun: () -> Unit)
}