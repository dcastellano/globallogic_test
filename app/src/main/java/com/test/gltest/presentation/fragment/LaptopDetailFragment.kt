package com.test.gltest.presentation.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.test.gltest.R
import com.test.gltest.extension.makeItGone
import com.test.gltest.extension.makeItVisible
import com.test.gltest.presentation.model.LaptopItem
import com.test.gltest.presentation.viewmodel.LaptopDetailViewModel
import com.test.gltest.presentation.viewmodel.LaptopState
import kotlinx.android.synthetic.main.fragment_laptop_detail.*
import kotlinx.android.synthetic.main.view_error.*
import kotlinx.android.synthetic.main.view_loading.*
import java.lang.Exception

class LaptopDetailFragment : BaseLaptopFragment() {

    companion object {
        private const val arg_title = "arg_title"

        fun newInstance(title: String) = LaptopDetailFragment().apply {
            val args = Bundle()
            args.putString(arg_title, title)
            arguments = args
        }
    }

    private var title = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        title = arguments?.getString(arg_title) ?: ""
        if (title.isEmpty()) {
            activity?.finish()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_laptop_detail, container, false)
    }

    override fun initViewModel() {
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(LaptopDetailViewModel::class.java)
        (viewModel as? LaptopDetailViewModel)?.laptopTitle = title
    }

    override fun onStateChanged(state: LaptopState) {
        super.onStateChanged(state)
        if (state is LaptopState.ItemReady)
            showData(state.item)
    }

    override fun showLoading() {
        laptop_details_content_container.makeItGone()
        view_error_container.makeItGone()
        view_loading_container.makeItVisible()
    }

    override fun showError() {
        laptop_details_content_container.makeItGone()
        view_error_container.makeItVisible()
        view_loading_container.makeItGone()
    }

    private fun showData(item: LaptopItem) {
        loadingImage(item.imageUrl)
        laptop_details_description.text = item.description
        laptop_details_content_container.makeItVisible()
        view_error_container.makeItGone()
        view_loading_container.makeItGone()
    }

    private fun loadingImage(url: String) {
        Picasso.get().load(url)
            .placeholder(R.drawable.ic_image_placeholder)
            .into(laptop_details_image, object : Callback{
                override fun onSuccess() {
                    laptop_details_image.scaleType = ImageView.ScaleType.CENTER_CROP
                }

                override fun onError(e: Exception?) {
                    laptop_details_image.scaleType = ImageView.ScaleType.FIT_CENTER
                }

            })
    }
}