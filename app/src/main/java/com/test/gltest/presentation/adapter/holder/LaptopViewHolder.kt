package com.test.gltest.presentation.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.test.gltest.R
import com.test.gltest.extension.makeItGone
import com.test.gltest.extension.makeItVisible
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.laptop_list_item.view.*

class LaptopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer,
    LaptopView {

    override val containerView = itemView

    override fun setTitle(text: String) {
        itemView.laptop_title.apply {
            this.text = text
            makeItVisible()
        }
    }

    override fun setShortDescription(text: String) {
        itemView.laptop_short_description.apply {
            this.text = text
            makeItVisible()
        }
    }

    override fun loadImage(picasso: Picasso, url: String) {
        itemView.apply {
            laptop_image_container.makeItVisible()
            picasso.load(url)
                .placeholder(R.color.gray)
                .resize(50, 50)
                .error(R.color.gray)
                .into(laptop_image)
        }
    }

    override fun hideTitle() = itemView.laptop_title.makeItGone()

    override fun hideShortDescription() = itemView.laptop_short_description.makeItGone()

    override fun hideImage() = itemView.laptop_image_container.makeItGone()

    override fun setOnItemClickListener(onClickFun: () -> Unit) =
        itemView.setOnClickListener { onClickFun() }
}