package com.test.gltest.presentation.viewmodel

import com.test.gltest.domain.interactor.LaptopInteractor
import com.test.gltest.presentation.viewmodel.LaptopState.ListReady

class LaptopViewModel(private val interactor: LaptopInteractor) : BaseLaptopViewModel() {

    override suspend fun getDataImpl(): LaptopState {
        val list = interactor.getLaptopList()
        return ListReady(list)
    }
}