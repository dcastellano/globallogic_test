package com.test.gltest.presentation.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.test.gltest.R
import com.test.gltest.presentation.fragment.LaptopDetailFragment
import com.test.gltest.presentation.fragment.LaptopFragment
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class LaptopDetailActivity : AppCompatActivity(), HasAndroidInjector {

    companion object {

        private const val extra_title = "extra_title"

        fun getIntent(context: Context, title: String): Intent {
            val intent = Intent(context, LaptopDetailActivity::class.java)
            intent.putExtra(extra_title, title)
            return intent
        }
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector() = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        val title = intent.extras?.getString(extra_title) ?: ""
        if (title.isEmpty()) {
            finish()
            return
        }

        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_default)

        supportFragmentManager.beginTransaction()
            .add(R.id.laptop_frame, LaptopDetailFragment.newInstance(title)).commit()
    }
}