package com.test.gltest.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.test.gltest.R
import com.test.gltest.presentation.adapter.holder.LaptopViewHolder
import com.test.gltest.presentation.adapter.presenter.LaptopHolderPresenter
import com.test.gltest.presentation.model.LaptopItem

class LaptopAdapter(
    val presenter: LaptopHolderPresenter,
    var list: List<LaptopItem> = listOf()
) : RecyclerView.Adapter<LaptopViewHolder>() {

    override fun getItemViewType(position: Int) = R.layout.laptop_list_item

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LaptopViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(viewType, parent, false)
    )

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: LaptopViewHolder, position: Int) =
        presenter.bind(holder, list[position])

    fun updateData(newList: List<LaptopItem>) {
        val result: DiffUtil.DiffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                presenter.areItemsTheSame(list[oldItemPosition], newList[newItemPosition])

            override fun getOldListSize() = list.size

            override fun getNewListSize() = newList.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                presenter.areContentsTheSame(list[oldItemPosition], newList[newItemPosition])
        })

        list = newList
        result.dispatchUpdatesTo(this)
    }
}