package com.test.gltest.presentation.adapter.presenter

import com.squareup.picasso.Picasso
import com.test.gltest.presentation.adapter.holder.LaptopView
import com.test.gltest.presentation.callback.LaptopItemCallback
import com.test.gltest.presentation.model.LaptopItem

class LaptopHolderPresenter(private val callback: LaptopItemCallback) {

    private val picasso = Picasso.get()

    fun bind(view: LaptopView, item: LaptopItem) {
        view.apply {
            if (item.title.isNotEmpty()) {
                setTitle(item.title)
            } else {
                hideTitle()
            }

            if (item.description.isNotEmpty()) {
                setShortDescription(item.description)
            } else {
                hideShortDescription()
            }

            if (item.imageUrl.isNotEmpty()) {
                loadImage(picasso, item.imageUrl)
            } else {
                hideImage()
            }

            setOnItemClickListener { callback.onItemClick(item.title) }
        }
    }

    fun areItemsTheSame(newItem: LaptopItem, oldItem: LaptopItem) =
        newItem.title.equals(oldItem.title, false)

    fun areContentsTheSame(newItem: LaptopItem, oldItem: LaptopItem) =
        newItem.description.equals(oldItem.description, true) &&
                newItem.imageUrl.equals(oldItem.imageUrl, true)
}