package com.test.gltest.presentation.model

data class LaptopItem(
    val title: String,
    val description: String,
    val imageUrl: String
)