package com.test.gltest.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.gltest.R
import com.test.gltest.extension.makeItGone
import com.test.gltest.extension.makeItVisible
import com.test.gltest.presentation.activity.LaptopDetailActivity
import com.test.gltest.presentation.adapter.LaptopAdapter
import com.test.gltest.presentation.adapter.presenter.LaptopHolderPresenter
import com.test.gltest.presentation.callback.LaptopItemCallback
import com.test.gltest.presentation.model.LaptopItem
import com.test.gltest.presentation.viewmodel.LaptopState
import com.test.gltest.presentation.viewmodel.LaptopState.*
import com.test.gltest.presentation.viewmodel.LaptopViewModel
import kotlinx.android.synthetic.main.fragment_laptop.*
import kotlinx.android.synthetic.main.view_error.*
import kotlinx.android.synthetic.main.view_loading.*

class LaptopFragment : BaseLaptopFragment(), LaptopItemCallback {

    companion object {
        fun newInstance() = LaptopFragment()
    }

    private val presenter = LaptopHolderPresenter(this)
    private val adapter = LaptopAdapter(presenter)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_laptop, container, false)
    }

    override fun onViewCreated() {
        f_laptop_list.layoutManager = LinearLayoutManager(context)
        f_laptop_list.adapter = adapter
        super.onViewCreated()
    }

    override fun initViewModel() {
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(LaptopViewModel::class.java)
    }

    override fun onStateChanged(state: LaptopState) {
        super.onStateChanged(state)
        if (state is ListReady)
            showData(state.list)
    }

    override fun showLoading() {
        f_laptop_content_container.makeItGone()
        view_error_container.makeItGone()
        view_loading_container.makeItVisible()
    }

    override fun showError() {
        f_laptop_content_container.makeItGone()
        view_error_container.makeItVisible()
        view_loading_container.makeItGone()
    }

    private fun showContent() {
        f_laptop_content_container.makeItVisible()
        view_error_container.makeItGone()
        view_loading_container.makeItGone()
    }

    private fun showData(list: List<LaptopItem>) {
        adapter.updateData(list)
        showContent()
    }

    override fun onItemClick(title: String) {
        context?.let { startActivity(LaptopDetailActivity.getIntent(it, title)) }
    }
}