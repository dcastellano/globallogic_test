package com.test.gltest.presentation.callback

interface LaptopItemCallback {
    fun onItemClick(title: String)
}