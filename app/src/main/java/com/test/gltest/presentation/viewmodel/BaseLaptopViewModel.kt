package com.test.gltest.presentation.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.gltest.presentation.model.LaptopItem
import kotlinx.coroutines.launch

abstract class BaseLaptopViewModel : ViewModel() {
    var state = MediatorLiveData<LaptopState>(); private set

    private fun setState(newState: LaptopState) {
        if (state.value != newState) {
            state.value = newState
        }
    }

    fun loadData() {
        setState(LaptopState.LoadingData)
        viewModelScope.launch {
            try {
                val state = getDataImpl()
                setState(state)
            } catch (e: Exception) {
                setState(LaptopState.DataError)
            }
        }
    }

    protected abstract suspend fun getDataImpl(): LaptopState
}

sealed class LaptopState {
    object LoadingData : LaptopState()
    data class ListReady(val list: List<LaptopItem>) : LaptopState()
    data class ItemReady(val item: LaptopItem) : LaptopState()
    object DataError : LaptopState()
}