package com.test.gltest.presentation.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.test.gltest.R
import com.test.gltest.presentation.fragment.LaptopFragment
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class LaptopActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector() = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_default)

        supportFragmentManager.beginTransaction()
            .add(R.id.laptop_frame, LaptopFragment.newInstance()).commit()
    }
}