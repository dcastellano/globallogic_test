package com.test.gltest.presentation.viewmodel

import com.test.gltest.domain.interactor.LaptopDetailInteractor
import com.test.gltest.presentation.viewmodel.LaptopState.ItemReady

class LaptopDetailViewModel(private val interactor: LaptopDetailInteractor) :
    BaseLaptopViewModel() {

    var laptopTitle = ""

    override suspend fun getDataImpl(): LaptopState {
        val item = interactor.getLaptop(laptopTitle)
        return ItemReady(item)
    }
}