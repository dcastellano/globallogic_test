package com.test.gltest.presentation.fragment

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.test.gltest.di.module.ViewModelFactory
import com.test.gltest.presentation.viewmodel.BaseLaptopViewModel
import com.test.gltest.presentation.viewmodel.LaptopState
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseLaptopFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    protected lateinit var viewModel: BaseLaptopViewModel

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        viewModel.state.observe(this, Observer(::onStateChanged))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewCreated()
    }

    open fun onViewCreated() {
        viewModel.loadData()
    }

    open fun onStateChanged(state: LaptopState) {
        when (state) {
            is LaptopState.LoadingData -> showLoading()
            is LaptopState.DataError -> showError()
        }
    }

    protected abstract fun initViewModel()
    protected abstract fun showLoading()
    protected abstract fun showError()
}