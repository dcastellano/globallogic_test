package com.test.gltest.repository.manager.laptop

import com.test.gltest.mapper.LaptopMapper
import com.test.gltest.repository.local.dao.LaptopDao
import com.test.gltest.repository.local.model.LaptopEntity
import com.test.gltest.repository.service.RetrofitService
import com.test.gltest.repository.service.model.LaptopDTO
import javax.inject.Inject

class LaptopRepoHelper @Inject constructor(
    private val service: RetrofitService,
    private val dao: LaptopDao,
    private val mapper: LaptopMapper
) {
    @Throws(Exception::class)
    suspend fun getServiceData() = service.getLaptopList()

    suspend fun getCacheData() = dao.getLaptopList()

    suspend fun getCacheData(title: String) = dao.getLaptop(title)

    fun mapList(list: List<LaptopDTO>) = mapper.toEntity(list)

    fun map(list: List<LaptopDTO>, title: String): LaptopEntity? {
        val item = list.firstOrNull { dto ->
            dto.title.equals(title, false)
        }
        return item?.let { dto -> mapper.toEntity(dto) }
    }

    suspend fun saveData(list: List<LaptopEntity>?) = list?.let { dao.insertLaptopList(it) }

    suspend fun saveData(item: LaptopEntity?) = item?.let { dao.insertLaptop(it) }
}