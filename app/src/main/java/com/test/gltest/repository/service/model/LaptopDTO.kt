package com.test.gltest.repository.service.model

import com.google.gson.annotations.SerializedName

data class LaptopDTO(
    @SerializedName("title")
    val title: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("image")
    val imageUrl: String?
)