package com.test.gltest.repository.manager.laptop

import com.test.gltest.repository.local.model.LaptopEntity
import com.test.gltest.repository.manager.model.RepositoryResult

interface LaptopRepoManager {
    suspend fun getLaptopList(): RepositoryResult<List<LaptopEntity>>
    suspend fun getLaptop(title: String): RepositoryResult<LaptopEntity>
}