package com.test.gltest.repository.manager.model

data class RepositoryResult<Result>(
    var exception: Exception? = null,
    var entity: Result? = null
)