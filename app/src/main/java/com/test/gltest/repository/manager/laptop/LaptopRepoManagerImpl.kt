package com.test.gltest.repository.manager.laptop

import com.test.gltest.repository.local.model.LaptopEntity
import com.test.gltest.repository.manager.RepoManager
import com.test.gltest.repository.manager.RepositoryStrategy
import com.test.gltest.repository.manager.model.RepositoryResult
import com.test.gltest.repository.service.model.LaptopDTO
import com.test.gltest.util.Util
import javax.inject.Inject

class LaptopRepoManagerImpl @Inject constructor(
    private val util: Util,
    private val helper: LaptopRepoHelper
) : LaptopRepoManager {

    override suspend fun getLaptopList(): RepositoryResult<List<LaptopEntity>> {
        val manager = object : RepoManager<List<LaptopDTO>, List<LaptopEntity>>(util) {
            override suspend fun getDto() = helper.getServiceData()
            override suspend fun getEntity() = helper.getCacheData()
            override fun map(dto: List<LaptopDTO>) = helper.mapList(dto)
            override suspend fun saveData(entity: List<LaptopEntity>?) {
                helper.saveData(entity)
            }
        }
        return manager.getResult(RepositoryStrategy.API)
    }

    override suspend fun getLaptop(title: String): RepositoryResult<LaptopEntity> {
        val manager = object : RepoManager<List<LaptopDTO>, LaptopEntity>(util) {
            override suspend fun getDto() = helper.getServiceData()
            override suspend fun getEntity() = helper.getCacheData(title)
            override fun map(dto: List<LaptopDTO>) = helper.map(dto, title)
            override suspend fun saveData(entity: LaptopEntity?) {
                helper.saveData(entity)
            }
        }
        return manager.getResult(RepositoryStrategy.CACHE)
    }
}