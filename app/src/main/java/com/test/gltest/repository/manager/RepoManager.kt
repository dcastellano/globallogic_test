package com.test.gltest.repository.manager

import android.util.Log
import com.test.gltest.repository.manager.model.RepositoryResult
import com.test.gltest.util.Util
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

abstract class RepoManager<DTO, Entity>(
    private val util: Util
) {

    val tag = RepoManager::class.java.simpleName

    suspend fun getResult(strategy: RepositoryStrategy)
            : RepositoryResult<Entity> = withContext(Dispatchers.IO) {
        val result: RepositoryResult<Entity> = if (strategy == RepositoryStrategy.API) {
            callApi()
        } else {
            fetchFromDB()
        }
        result
    }

    private suspend fun callApi(): RepositoryResult<Entity> {
        return if (!isNetworkConnected()) {
            onApiCallError("No internet connection")
        } else {
            fetchDto()?.let { onApiCallSuccess(it) }
                ?: run { onApiCallError("Something went wrong calling the API") }
        }
    }

    private fun isNetworkConnected() = util.isNetworkAvailable()

    private suspend fun onApiCallSuccess(dto: DTO): RepositoryResult<Entity> {
        val entity = map(dto)
        saveData(entity)
        return fetchFromDB()
    }

    private suspend fun onApiCallError(message: String): RepositoryResult<Entity> {
        val exception = Exception(message)
        Log.e(tag, "${exception.message}")
        return fetchFromDB(exception)
    }

    private suspend fun fetchDto(): DTO? {
        return try {
            getDto()
        } catch (t: java.lang.Exception) {
            Log.e(tag, "OnError calling API: ${t.message}")
            null
        }
    }

    private suspend fun fetchFromDB(exception: Exception? = null): RepositoryResult<Entity> {
        val cacheData = getEntity()
        cacheData?.let { return RepositoryResult(entity = it, exception = exception) }

        val errorMessage = "LocalDataBaseException: Invalid entity"
        Log.e(tag, errorMessage)
        return RepositoryResult(exception = Exception(errorMessage))
    }

    @Throws(Throwable::class)
    protected abstract suspend fun getDto(): DTO?

    protected abstract suspend fun getEntity(): Entity?

    protected abstract fun map(dto: DTO): Entity?

    protected abstract suspend fun saveData(entity: Entity?)
}

enum class RepositoryStrategy { API, CACHE }