package com.test.gltest.repository.service

import com.test.gltest.repository.service.model.LaptopDTO
import retrofit2.http.GET

interface RetrofitService {

    @GET("list")
    suspend fun getLaptopList(): List<LaptopDTO>
}