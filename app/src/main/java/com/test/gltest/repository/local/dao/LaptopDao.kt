package com.test.gltest.repository.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.gltest.repository.local.model.LaptopEntity

@Dao
abstract class LaptopDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertLaptopList(list: List<LaptopEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertLaptop(item: LaptopEntity)

    @Query("SELECT * FROM laptop_table")
    abstract suspend fun getLaptopList(): List<LaptopEntity>

    @Query("SELECT * FROM laptop_table WHERE title = :title LIMIT 1")
    abstract suspend fun getLaptop(title: String): LaptopEntity?
}