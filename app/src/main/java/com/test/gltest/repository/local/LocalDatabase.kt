package com.test.gltest.repository.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.gltest.repository.local.dao.LaptopDao
import com.test.gltest.repository.local.model.LaptopEntity

@Database(
    entities = [LaptopEntity::class],
    version = 1,
    exportSchema = false
)
abstract class LocalDataBase : RoomDatabase() {
    abstract val laptopDao: LaptopDao
}