package com.test.gltest.mapper

import com.test.gltest.presentation.model.LaptopItem
import com.test.gltest.repository.local.model.LaptopEntity
import com.test.gltest.repository.service.model.LaptopDTO

interface LaptopMapper {
    fun toPresentation(list: List<LaptopEntity>, shortDescriptionMaxChar: Int): List<LaptopItem>
    fun toPresentation(item: LaptopEntity): LaptopItem
    fun toEntity(list: List<LaptopDTO>): List<LaptopEntity>
    fun toEntity(item: LaptopDTO): LaptopEntity?
}