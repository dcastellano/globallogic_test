package com.test.gltest.mapper

import com.test.gltest.presentation.model.LaptopItem
import com.test.gltest.repository.local.model.LaptopEntity
import com.test.gltest.repository.service.model.LaptopDTO
import javax.inject.Inject

class LaptopMapperImpl @Inject constructor() : LaptopMapper {

    override fun toPresentation(list: List<LaptopEntity>, shortDescriptionMaxChar: Int) =
        list.map { item ->
            val shortDescription = getShortDescription(item.description, shortDescriptionMaxChar)
            mapItem(item, shortDescription)
        }

    override fun toPresentation(item: LaptopEntity) = LaptopItem(
        title = item.title,
        description = item.description,
        imageUrl = item.imageUrl
    )

    override fun toEntity(list: List<LaptopDTO>): List<LaptopEntity> = list.mapNotNull { dto ->
        if (dto.title.isNullOrEmpty())
            return@mapNotNull null
        mapEntity(dto.title, dto.description, dto.imageUrl)
    }

    override fun toEntity(item: LaptopDTO): LaptopEntity? {
        return if (item.title.isNullOrEmpty())
            null
        else
            mapEntity(item.title, item.description, item.imageUrl)
    }

    fun mapItem(item: LaptopEntity, shortDescription: String) = LaptopItem(
        title = item.title,
        description = shortDescription,
        imageUrl = item.imageUrl
    )

    fun getShortDescription(description: String, shortDescriptionMaxChar: Int): String {
        if (shortDescriptionMaxChar > description.length)
            return description
        return description.substring(0, shortDescriptionMaxChar)
    }

    fun mapEntity(title: String, description: String?, imageUrl: String?) = LaptopEntity(
        title = title,
        description = description ?: "",
        imageUrl = imageUrl ?: ""
    )
}