# README #

- This project is based on Kotlin (Original request was Java but after asking for a changed, Kotlin was confirmed)
- Unit Test was performed only in LaptopHolderPresenter as an example of it. The project was structured in a way to make unit test easier.
- MVVM patter was selected for interacting with Fragments.
- There was also applied MVP pattern for interacting with List Adapter (LaptopHolderPresenter)